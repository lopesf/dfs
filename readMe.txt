DFS with:

Assuming well behaved user, who will OPEN a file before READ its contents, then WRITE new contents, and close optional

(1) Distributed transparent file access
(2) Directory Server
	Each server registers at start-up
	Each client asks directory for server 'contact card' for each file at OPEN stage
	If it is a new file, directory will assign a server to handle it, and tell the server
(3) Cache
	At client side, using time stamps. Only reading file contents if cache is out of date
	or contents not in cache;
	User sends ping message to server with time stamp
(4) Locking
	Happens transparently, without the client knowning about;
	Lease method used, where token expires after 30 seconds;
	30 seconds lease can cause delays but it's used for testing purposes.
	
Classes:

(1) Client.java				// client class
	ClientCache.java		// client cache class, uses ClientFileRecord
	ClientFileRecord.java	// record of a file on the client side

(2) DirectoryServer.java	// main directory server using thread pool
	DSWorker.java			// thread worker class for a thread contacting directory server
	DSRecord.java			// records used by the directory server

(3) Server.java				// file Server class using thread pool
	ServerWorker.java		// thread worker class for a thread contacting the file server
	ServerFileRecord.java	// records for a file used by the server
	ServerCache.java		// server memory, using ServerFileRecord objects

(4) ServerLockService.java	// Lock service used by file server to generate token, lock allocation, admin
	ServerLockRecord.java	// records of a token / owner, used by the ServerLockService