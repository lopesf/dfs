import java.sql.Timestamp;
import java.util.ArrayList;


public class ClientCache {

	
	private ArrayList<ClientFileRecord> files;
	
	public ClientCache() {
		files = new ArrayList<ClientFileRecord>(10);
	}
	
	public int indexInCache(String filename) {
		for(int i = 0; i < files.size(); i++) {
			if(files.get(i).filename.equals(filename)) return i;
		}
		return -1;
	}
	
	public boolean checkContent(int index, int start, int length) {
		
			if(files.get(index).startPos <= start &&
					(files.get(index).startPos + files.get(index).length) >= (start + length)) return true;
			else  return false;

	}
	
	public Timestamp getTimestamp(int fileIndex) {
		return files.get(fileIndex).lastWriteTimestamp;
	}
	
	public String getToken(int index) {
		return files.get(index).token;
	}
	
	public void setToken(int index, String token) {
		files.get(index).token = token;
	}
	
	public int add(String filename, String ip, String port) {
		files.ensureCapacity(files.size()+1);
		files.add(new ClientFileRecord(filename, ip, port));
		return (files.size() - 1);	// index of entry in cache
	}
	
	public String[] getServerDetails(int index) {
		String[] result = {files.get(index).serverIp, files.get(index).serverPort};
		return result;
	}
	
	public String read(int index) { return files.get(index).read(); }
	
	public void write(int index, String content, int startPos, int length, Timestamp t) {
		files.get(index).write(content, startPos, length, t);
	}
	
	
	
	
}
