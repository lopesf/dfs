import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;


public class ServerLockRecord {

	public String filename;
	public String currentToken;
	public Timestamp expiry;
	public ArrayList<String> waitingQueue;
	
	private final int lease = 30;		// 30 second lease
	
	public ServerLockRecord(String filename, String ownerToken) {
		this.filename = filename;
		this.currentToken = ownerToken;
		waitingQueue = new ArrayList<String>();
		this.expiry = generateExpiryTimestamp();
	}
	
	public void addToQueue(String user) {
		this.waitingQueue.ensureCapacity(this.waitingQueue.size()+1);
		this.waitingQueue.add(user);
	}
	
	// Returns token of current holder of the lock
	// If expired moves up the queue
	// threads will wait in a spin loop calling this method
	public String currentHolder() {
		Timestamp now = now();
		if(now.before(expiry)) 
			return this.currentToken;
		else {
			if(this.waitingQueue.isEmpty())
				return "NULL";
			else {
				this.currentToken = this.waitingQueue.remove(0);
				this.expiry = generateExpiryTimestamp();
				return this.currentToken;
			}
		}
	}
	
	public void release(String user) {
		if(this.currentToken.equals(user))
			this.expiry = new Timestamp(0);
	}
	
	private Timestamp now() {
		Date date = new Date();
		return (new Timestamp(date.getTime()));		
	}
	
	private Timestamp generateExpiryTimestamp() {
		Date date = new Date();
		return (new Timestamp(date.getTime() + lease * 1000));
	}
}
