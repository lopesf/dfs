import java.util.ArrayList;


public class ServerLockService {

	public ArrayList<ServerLockRecord> locks;
	
	public ServerLockService() {
		
		locks = new ArrayList<ServerLockRecord>();
	}
	
	public String acquire(String filename, String user) {
		int index = indexForLockRecord(filename);
		if(index == -1) {
			locks.add(new ServerLockRecord(filename, user));
			return user;
		}
		else {
			locks.get(index).addToQueue(user);
			return locks.get(index).currentHolder();
		}
	}
	
	public String spin(String filename) {
		int index = indexForLockRecord(filename);
		return locks.get(index).currentHolder();
	}
	
	public void release(String filename, String user) {
		int index = indexForLockRecord(filename);
		locks.get(index).release(user);
	}
	
	private int indexForLockRecord(String filename) {
		for(int i = 0; i < locks.size(); i++) {
			if(locks.get(i).filename.equals(filename))
				return i;
		}
		return -1;
	}
}
