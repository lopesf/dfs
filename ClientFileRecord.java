import java.sql.Timestamp;

public class ClientFileRecord {

	public String filename;
	private String content;
	public int startPos;
	public int length;
	public Timestamp lastWriteTimestamp;
	public String token;
	
	public String serverIp;
	public String serverPort;
	
	
	public ClientFileRecord(String filename, String ip, String port) {
		
		this.filename = filename;
		this.lastWriteTimestamp = new Timestamp(0);
		this.content = "(blank)";
		this.startPos = this.length = 0;
		this.serverIp = ip; this.serverPort = port;
	}
	
	public String read() { return content; }
	
	public void write(String content, int start, int length, Timestamp t) {
		this.content = content;
		this.startPos = start;
		this.length = length;
		this.lastWriteTimestamp = t;
		
	}

}
