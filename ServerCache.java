import java.util.ArrayList;


public class ServerCache {

	public ArrayList<ServerFileRecord> files;
	
	public ServerCache() {
		files = new ArrayList<ServerFileRecord>(10);
	}
	
	public int indexInCache(String filename) {
		for(int i = 0; i < files.size(); i++) {
			if(files.get(i).filename.equals(filename))
				return i;
		}
		return -1;
	}
	
	public void addNewFile(String filename) {
		files.ensureCapacity(files.size()+1);
		files.add(new ServerFileRecord(filename));
	}
	
}
