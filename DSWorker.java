import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Random;


public class DSWorker implements Runnable{

    protected 	Socket clientSocket = null;
    protected 	String serverText   = null;
    protected	DirectoryServer server = null;

    private		ArrayList<DSRecord> records = null;

    private 	BufferedReader 	is;
    private		PrintStream		os;


    public DSWorker(Socket clientSocket, String serverText, DirectoryServer server, ArrayList<DSRecord> records) {
        this.clientSocket = clientSocket;
        this.serverText   = serverText;
        this.server = server;
        this.records = records;

	
    }

	public void run() {
		
    	ArrayList<DSRecord> records = this.records;
    	String delimiter = "//@delimiter@//";

        try {
        	
		is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		os = new PrintStream(clientSocket.getOutputStream());
        	
		String	message = is.readLine();

		
		
		if(message.startsWith("SERVER_SIGNUP")) {
			
			String ip = clientSocket.getLocalAddress().getHostAddress().toString();
			int port = this.clientSocket.getLocalPort();
			int serverId;
			
			DSRecord newServer = new DSRecord(ip, port);
			
			synchronized(this) {
				records.ensureCapacity(records.size()+1);
				records.add(newServer);
				serverId = records.size() - 1;			// position in array is server identifier
			}
			
			os.println("OK: Added to directory"+delimiter+"Ip: "+ip+delimiter+"Port: "+port+delimiter+serverId);
		}
		else {
			String[] messageSplit = message.split(delimiter);
			
			boolean success = false;
			// SERVER_ADD delimiter SERVER_ID delimiter FILENAME
			if(message.startsWith("SERVER_ADD")) {
				synchronized(this) {
					success = records.get(Integer.parseInt(messageSplit[1])).serverAddRecord(messageSplit[2]);
				}
				if(success)
					os.println("DS_SERVER: Added record for "+messageSplit[2] + " to ServerId "+messageSplit[1]);
				else
					os.println("DS_SERVER: Error 01, ServerId "+messageSplit[1] + " already contains "+messageSplit[2]);
			}
			// SERVER_REMOVE delimiter SERVER_ID delimiter FILENAME
			else if(message.startsWith("SERVER_REMOVE")) {
				synchronized(this) {
					success = records.get(Integer.parseInt(messageSplit[1])).serverRemoveRecord(messageSplit[2]);
				}
				if(success)
					os.println("DS_SERVER: Removed record for "+messageSplit[2] + " to ServerId "+messageSplit[1]);
				else
					os.println("DS_SERVER: Error 02, ServerId "+messageSplit[1] + " does not contain record "+messageSplit[2]);
			}
			// CLIENT delimiter FILENAME
			else if(message.startsWith("CLIENT")) {
				
				boolean found = false;
				
				synchronized(this) {
					for(int i = 0; i < records.size(); i++) {
						if(records.get(i).clientRequest(messageSplit[1])) {
							found = true; 
							os.println("OK"+delimiter+records.get(i).serverIp+delimiter+records.get(i).serverPort);	
							break;
						}
					}
				}
				
				if(! found) {
			//      os.println("DS_SERVER: File "+messageSplit[1]+" not found");
					/*
					 * New file not in any server, assign a server at random
					 * Another option would be to ping message servers to assert
					 * availability of service to accommodate a new file
					 */
					Random randGenerator = new Random();
					synchronized(this){
						int randomServer = randGenerator.nextInt(records.size());
						records.get(randomServer).serverAddRecord(messageSplit[1]);	// add filename to a server
						os.println("OK"+delimiter+records.get(randomServer).serverIp+delimiter+records.get(randomServer).serverPort);
						is.close(); os.close();
						
						// then tell server of new file
						// DIRECTORY_NEWFILE FILENAME
						Socket directorySocket = new Socket(directoryServerIp, directoryServerPort);
						os = new PrintStream(directorySocket.getOutputStream());
						is = new BufferedReader(new InputStreamReader(directorySocket.getInputStream()));
						
						os.println("DIRECTORY_NEWFILE" + delimiter + messageSplit[1]);
						String reply = is.readLine();
						if(!reply.startsWith("OK")) 
							System.out.println("Error registering new file");
						
						directorySocket.close();
					}
										
				}
					
				
			}
			else {
				os.println("DS_SERVER: Error 03: Invalid message format.");
			}
		}
      
        is.close();
        os.close();
        } catch (Exception e) {}
    } // end of run method

}
