import java.util.ArrayList;

/*
 * For this solution assumed that human readable
 * global filenames correspond to the name of the
 * files on a server
 */

public class DSRecord {

	public String serverIp;
	public int serverPort;
	private ArrayList<String> filenames;
//	private ArrayList<String> fileIdentifiers;
	
	public DSRecord(String ip, int port) {
		this.serverIp = ip;
		this.serverPort = port;
		this.filenames = new ArrayList<String>(20);
// 		this.fileIdentifiers = new ArrayList<String>(20);
	}
	
	public boolean serverAddRecord(String filename) {
		if(this.filenames.contains(filename))
			return false;
		this.filenames.ensureCapacity(this.filenames.size()+1);
		this.filenames.add(filename);
		return true;
	}
	
	public boolean serverRemoveRecord(String filename) {
		return this.filenames.remove(filename);
	}
	
	public boolean clientRequest(String filename) {
		return this.filenames.contains(filename);
	}
	
}
