import java.io.*;
import java.net.*;
import java.sql.Timestamp;



public class ServerWorker implements Runnable{

    protected 	Socket 				clientSocket = null;
    protected	Server 				server = null;

    private 	BufferedReader 		is;
    private		PrintStream			os;
    
    private		String				serverId;
    private		ServerCache			cache;
    private		ServerLockService 	locks;


    public ServerWorker(Socket clientSocket, Server server, String serverId, ServerCache cache, ServerLockService locks) {
        this.clientSocket = clientSocket;
        this.server = server;
        this.serverId = serverId;
        this.cache = cache;
        this.locks= locks;
    }

	public void run() {

    	String serverId = this.serverId;
    	ServerCache cache = this.cache;
    	ServerLockService locks = this.locks;
    	
    	String delimiter = "//@delimiter@//";
    	

        try {
        	/*		Each component is separated by a delimiter
        	 * (1) OPEN	FILENAME 
        	 * 		Server: OK FILENAME TOKEN		(spin lock is implemented (wait loop))
        	 * 
        	 * (2) CLOSE FILENAME TOKEN
        	 * 		Server: OK FILENAME
        	 * 
        	 * (3) READ FILENAME TOKEN START-POS LENGTH		
        	 * 		Server: OK FILENAME CONTENT TIMESTAMP	
        	 * 
        	 * (4) WRITE FILENAME TOKEN				(overwrite assumed)
        	 * 		Server: OK FILENAME
        	 * 
        	 * (5) TIMESTAMP_CHECK FILENAME TIMESTAMP
        	 * 		Server:	OK FILENAME
        	 * 				OUT_OF_DATE FILENAME
        	 * 
        	 * (6) DIRECTORY_NEWFILE FILENAME
        	 */
        	
			is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			os = new PrintStream(clientSocket.getOutputStream());
	        	
			String	request = is.readLine();
			String[] requestSplit = request.split(delimiter);
			
			if(request.startsWith("OPEN")) {
				// OPEN	FILENAME 
				int indexOfRecord = cache.indexInCache(requestSplit[1]);
				if(indexOfRecord == -1)
					os.println("Error 01: File not registered on this server");
				else {
					String userToken = this.toString();
					String currentHolder;
					
					//attempt to lock file
					synchronized(this) {
						currentHolder = locks.acquire(requestSplit[1], userToken);
					}
					//while not locked - spin wait loop
					while(!currentHolder.equals(userToken)) {
						synchronized(this) {
							currentHolder = locks.spin(requestSplit[1]);
						}
					}
					// OK FILENAME TOKEN
					os.println("OK "+ delimiter + requestSplit[1] + delimiter + userToken);
				}
			}
			else if(request.startsWith("CLOSE")) {
				// CLOSE FILENAME TOKEN
				os.println("OK "+ requestSplit[1]);
				// release lock if still valid and not yet released
				synchronized(this) {
					locks.release(requestSplit[1], requestSplit[2]);
				}
			}
			else if(request.startsWith("READ")) {
				// READ FILENAME START-POS LENGTH TOKEN
				String filename = requestSplit[1];
				int startPos = Integer.parseInt(requestSplit[2]);
				int length = Integer.parseInt(requestSplit[3]);
				String token = requestSplit[4];
				String currentLockHolder;
				//check if token still valid
				synchronized(this) {
					currentLockHolder = locks.spin(filename);
				}
				
				if(!currentLockHolder.equals(token)) {
					// try acquire again and wait
					synchronized(this) {
						currentLockHolder = locks.acquire(filename, token);
					}
					while(!currentLockHolder.equals(token)) {
						synchronized(this) {
							currentLockHolder = locks.spin(filename);
						}
					}
				}
				// have the lock so go ahead and read
				synchronized(this) {
					int indexOfRecord = cache.indexInCache(filename);
					int[] bounds = cache.files.get(indexOfRecord).readBoundsCheck(startPos, length);
					String contentRead = cache.files.get(indexOfRecord).read(startPos, length);
					
					//OK FILENAME CONTENT TIMESTAMP START LENGTH
					os.println("OK" + delimiter + filename + delimiter + contentRead + delimiter +
							cache.files.get(indexOfRecord).lastWritten + delimiter + bounds[0] + 
							delimiter + bounds[1]);
				}// end sync
			}// end read
			else if(request.startsWith("WRITE")) {
				// WRITE FILENAME CONTENT TOKEN
				String filename = requestSplit[1];
				String content = requestSplit[2];
				String token = requestSplit[3];
				String currentLockHolder;
				//check if token still valid
				synchronized(this) {
					currentLockHolder = locks.spin(filename);
				}
				
				if(!currentLockHolder.equals(token)) {
					// try acquire again and wait
					synchronized(this) {
						currentLockHolder = locks.acquire(filename, token);
					}
					while(!currentLockHolder.equals(token)) {
						synchronized(this) {
							currentLockHolder = locks.spin(filename);
						}
					}
				}
				// have the lock so go ahead and read
				
				synchronized(this) {
					int indexOfRecord = cache.indexInCache(filename);
					cache.files.get(indexOfRecord).write(content);
					
					// OK FILENAME TIMESTAMP
					os.println("OK" + delimiter + filename + delimiter + cache.files.get(indexOfRecord).lastWritten);
				} // end sync
			} // end write
			else if(request.startsWith("TIMESTAMP")) {
				// TIMESTAMP_CHECK FILENAME TIMESTAMP
				String filename = requestSplit[1];
				Timestamp clientTimestamp = Timestamp.valueOf(requestSplit[2]);
				boolean valid = false;
				synchronized(this) {
					int indexOfRecord = cache.indexInCache(filename);
					valid = cache.files.get(indexOfRecord).checkTimestamp(clientTimestamp);
				}
				if(valid)
					os.println("OK" + delimiter + filename);
				else
					os.println("OUT_OF_DATE" + delimiter + filename);
			}
			else if(request.startsWith("DIRECTORY")) {
				// DIRECTORY_NEWFILE FILENAME
				String filename = requestSplit[1];
				synchronized(this) {
					cache.addNewFile(filename);
				}
				os.println("OK" + delimiter + filename + delimiter + serverId);
			}
	        is.close(); os.close();
        } catch (Exception e) {}
    }
	


}
