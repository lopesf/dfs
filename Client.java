import java.io.*;
import java.net.Socket;
import java.sql.Timestamp;



public class Client {
	

	static String delimiter = "//@delimiter@//";
	
	static String directoryServerIp = "localhost";
	static int directoryServerPort = 2222;
	
	private static String getUserRequest() throws Exception {
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Main menu:\nEnter OPEN/READ/WRITE/CLOSE " +
				"followed by filename\n(ie OPEN afile.txt)\n" +
				"Your choice---> ");
		String request = inFromUser.readLine();
		request = request.replace(" ", delimiter);
		return request;
	}
	
	public static void main(String args[]) throws Exception {
		
		Socket clientSocket;
		PrintStream os;
		BufferedReader is;
		boolean exit = false;
		
		ClientCache cache = new ClientCache();
		
		while(true) {
			
			try{
				String userRequest = getUserRequest();
				String[] userRequestSplit = userRequest.split(delimiter);;
				
				String response;
				String[] responseSplit;
				
				// OPEN FILENAME
				if(userRequestSplit[0].equals("OPEN")) {
					// get server details from directory
					// if new file, directory will assign a server for it
					// randomly, but could ping servers for availability
					
					int fileIndex = cache.indexInCache(userRequestSplit[1]);
					if(fileIndex == -1){
						// if not in cache: new file so contact directory for details
						// and create record on cache
						Socket directorySocket = new Socket(directoryServerIp, directoryServerPort);
						os = new PrintStream(directorySocket.getOutputStream());
						is = new BufferedReader(new InputStreamReader(directorySocket.getInputStream()));
						
						os.println("CLIENT"+delimiter+userRequestSplit[1]);
						response = is.readLine();
						
						os.close(); is.close(); directorySocket.close();
						
						// OK SERVER_IP SERVER_PORT
						if(!response.startsWith("OK")) 
							System.out.println("Error 17: File not found!\n DS Response: "+ response);
						else{
							// directory server replied with IP and port details
							responseSplit = response.split(delimiter);
							fileIndex = cache.add(userRequestSplit[1], responseSplit[1], responseSplit[2]);
						}
					}
					// contact server holding the file with request
					String[] serverDetails = cache.getServerDetails(fileIndex); // returns [IP] [port]
					
					// open socket and streams to server
					clientSocket = new Socket(serverDetails[0], Integer.parseInt(serverDetails[1]));
					os = new PrintStream(clientSocket.getOutputStream());
					is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
					
					os.println(userRequest);
					response = is.readLine();
					responseSplit = response.split(delimiter);
					// OK FILENAME TOKEN
					if(response.startsWith("OK")) {
						System.out.println("File "+ userRequestSplit[1] + " is now open!");
						cache.setToken(fileIndex, responseSplit[2]);
					}
						
					else
						System.out.println("Error closing the file C-02");
					os.close(); is.close(); clientSocket.close();
				}
				
				// CLOSE FILENAME
				else if(userRequest.startsWith("CLOSE")) {
					
					// get contact details from cache
					int fileIndex = cache.indexInCache(userRequestSplit[1]);
					String[] serverDetails = cache.getServerDetails(fileIndex); 
					
					// open socket and streams to server
					clientSocket = new Socket(serverDetails[0], Integer.parseInt(serverDetails[1]));
					os = new PrintStream(clientSocket.getOutputStream());
					is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
					
					// tell server holding the file
					// CLOSE FILENAME TOKEN
					os.println(userRequest + delimiter + cache.getToken(fileIndex));
					response = is.readLine();
					if(response.startsWith("OK")) 
						System.out.println("File "+ userRequestSplit[0] + " is now closed!");
					else
						System.out.println("Error closing the file C-02");
					os.close(); is.close(); clientSocket.close();
				}
				
				// READ FILENAME START-POS LENGTH
				else if(userRequest.startsWith("READ")) {
					
					// assume user will open a file before trying to read it
					// opening a file adds 'empty' record to cache, w/ time stamp 1970-01-01 (expired)
					// thus options here are (1) file up to date in cache; (2) not up to date
					
					int indexInCache = cache.indexInCache(userRequestSplit[1]); // must exist
					
					// get server contact details for file requested
					String[] serverDetails = cache.getServerDetails(indexInCache); 
					
					// open socket and streams to server holding the file
					clientSocket = new Socket(serverDetails[0], Integer.parseInt(serverDetails[1]));
					os = new PrintStream(clientSocket.getOutputStream());
					is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
					
					
					// check length and start position in cache match requested
					if(cache.checkContent(indexInCache, Integer.parseInt(userRequestSplit[2]), Integer.parseInt(userRequestSplit[3]))) {
							
						// send ping to server with time stamp on cache
						// TIMESTAMP_CHECK FILENAME TIMESTAMP
						String ping = "TIMESTAMP_CHECK" + delimiter + userRequestSplit[1] + 
								delimiter + cache.getTimestamp(indexInCache);
						// ping message sent
						os.println(ping);
						response = is.readLine();
						responseSplit = response.split(delimiter);
						
						if(response.startsWith("OK"))
							// OK FILENAME
							System.out.println(userRequestSplit[1] + " (read from cache):\n"+cache.read(indexInCache));
						else {
							// OUT_OF_DATE
							os.println(userRequest + delimiter + cache.getToken(indexInCache));	// READ FILENAME START-POS LENGTH TOKEN
							//OK FILENAME CONTENT TIMESTAMP START LENGTH
							response = is.readLine();
							responseSplit = response.split(delimiter);
							if(response.startsWith("OK")) {
								//update cache
								cache.write(indexInCache, responseSplit[2], Integer.parseInt(responseSplit[4]), Integer.parseInt(responseSplit[5]), Timestamp.valueOf(responseSplit[3]));
								System.out.println(userRequestSplit[1] + " (from server - cache updated (1)) : "+ responseSplit[2]);
							}
							else
								System.out.println("ERROR 14: Error reading content from file\nSERVER RESPONSE: "+ response);
						}
					}
					else {
						// case where content start position / length not included in cache
						os.println(userRequest + delimiter + cache.getToken(indexInCache));			// READ FILENAME START-POS LENGTH TOKEN
						//OK FILENAME CONTENT TIMESTAMP START LENGTH
						response = is.readLine();
						responseSplit = response.split(delimiter);
						if(response.startsWith("OK")) {
							//update cache
							cache.write(indexInCache, responseSplit[2], Integer.parseInt(responseSplit[4]), Integer.parseInt(responseSplit[5]), Timestamp.valueOf(responseSplit[3]));
							System.out.println(userRequestSplit[1] + " (from server - cache updated (2)) : "+ responseSplit[2]);
						}
						else
							System.out.println("ERROR 14: Error reading content from file\nSERVER RESPONSE: "+ response);
					}
					os.close(); is.close(); clientSocket.close();
				}
				else if(userRequest.startsWith("WRITE")) {
					
					int indexInCache = cache.indexInCache(userRequestSplit[1]); // must exist
					
					// get server contact details for file requested
					String[] serverDetails = cache.getServerDetails(indexInCache); 
					
					// open socket and streams to server
					clientSocket = new Socket(serverDetails[0], Integer.parseInt(serverDetails[1]));
					os = new PrintStream(clientSocket.getOutputStream());
					is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
					
					// assume override
					// WRITE FILENAME CONTENT TOKEN
					os.println(userRequest + delimiter + cache.getToken(indexInCache));
					// OK FILENAME TIMESTAMP
					response = is.readLine();
					responseSplit = response.split(delimiter);
					if(response.startsWith("OK")) {
						System.out.println("File "+ userRequestSplit[1] + " write successful!");
						// update local cache
						cache.write(indexInCache, userRequestSplit[2], 0, userRequestSplit[2].length(), Timestamp.valueOf(responseSplit[2]));
					}	
					else
						System.out.println("Error closing the file C-02");	
					os.close(); is.close(); clientSocket.close();
				}
				else if(userRequest.startsWith("EXIT")) {
					exit = true;
				}
				else {
					System.out.println("Invalid message format.");
				}
				
				
				if(exit) break;
				
			}catch(Exception e){}
		} // end while
		
		
	}// end main
}
