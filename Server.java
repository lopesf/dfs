import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Server implements Runnable {

    protected int          		serverPort;
    protected ServerSocket 		serverSocket 	= null;
    protected boolean      		isStopped    	= false;
    protected Thread       		runningThread	= null;
    protected ExecutorService 	threadPool 		= Executors.newFixedThreadPool(50);
    
    private	  String			serverId		= "";
    private   ServerCache		cache			= new ServerCache();
    private	  ServerLockService locks			= new ServerLockService();
	
    public Server(int port){
        this.serverPort = port;
    }
	
	

	public void run() {
		
		try{
			/*
			 * Contact directory server to register
			 * and pass details of any files on this server
			 */
	    	String directoryServerIp = "localhost";
	    	int directoryServerPort = 2222;
	    	String delimiter = "//@delimiter@//";

			Socket directorySocket = new Socket(directoryServerIp, directoryServerPort);
			PrintStream os = new PrintStream(directorySocket.getOutputStream());
			BufferedReader is = new BufferedReader(new InputStreamReader(directorySocket.getInputStream()));
			
			os.println("SERVER_SIGNUP");
			String response = is.readLine();
			
			if(response.startsWith("OK")) {
				System.out.println(response);
				String[] responseSplit = response.split(delimiter);
				this.serverId = responseSplit[3];
				/*
				 * Here register files on server
				 * os.println("SERVER_ADD" + delimiter + this.serverId + delimiter + 'FILENAME');
				 * 
				 * If user enters a new file not in the directory, the directory will assign a server
				 * for it at random, another possibility would be to ping servers for availability to
				 * accommodate a new file
				 */
				os.close(); is.close(); directorySocket.close();	// close communication with directory server
			}
				
			else
				System.out.println("Error registering with directory server");

		}catch(Exception e){}

		synchronized(this){
            this.runningThread = Thread.currentThread();
        }
        openServerSocket();
        while(! isStopped()){
            Socket clientSocket = null;
            try {
                clientSocket = this.serverSocket.accept();
            } catch (IOException e) {
                if(isStopped()) {
                    System.out.println("Server Stopped.") ;
                    return;
                }
                throw new RuntimeException(
                    "Error accepting client connection", e);
            }
            this.threadPool.execute(
                new ServerWorker(clientSocket, this, this.serverId, cache, locks));

        }
        this.threadPool.shutdown();
        System.out.println("Server Stopped.") ;
    }


    private synchronized boolean isStopped() {
        return this.isStopped;
    }

    public synchronized void stop(){
        this.isStopped = true;
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            throw new RuntimeException("Error closing server", e);
        }
    }

    private void openServerSocket() {
        try {
            this.serverSocket = new ServerSocket(this.serverPort);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port "+ this.serverPort, e);
        }
    }
	public static void main(String[] args) {
		Server server = new Server(Integer.parseInt(args[0]));
//		Server server = new Server(1234);
		new Thread(server).start();

	}
	
}
