import java.util.Date;
import java.sql.Timestamp;


public class ServerFileRecord {

	public String filename;
	private String content;
	public Timestamp lastWritten;
	
	public ServerFileRecord(String filename) {
		this.filename = filename;
		this.content = "(blank)";
		this.lastWritten = new Timestamp(0);	
	}
	
	public ServerFileRecord(String filename, String content) {
		this.filename = filename;
		this.content = content;
		this.lastWritten = generateTimestamp();
	}
	
	public String read(int startPos, int length) {
		if(startPos + length <= content.length())
			return this.content.substring(startPos, startPos + length);
		else
			return this.content.substring(startPos);
	}
	
	public int[] readBoundsCheck(int startPos, int length) {
		int[] result = new int[2];
		if(startPos + length <= content.length()) {
			result[0] = startPos; result[1] = startPos + length;
		}
		else {
			result[0] = startPos; result[1] = this.content.length() - startPos;
		}
		return result;
	}
	
	public void write(String content) {
		this.content = content;
		this.lastWritten = generateTimestamp();
	}
	
	
	
	private Timestamp generateTimestamp() {
		Date date = new Date();
		return (new Timestamp(date.getTime()));
	}
	
	public boolean checkTimestamp(Timestamp clientTimestamp) {
		return this.lastWritten.equals(clientTimestamp);
	}
	
}
